# Техподдержка

Если вы пишете в чат поддержки, то, пожалуйста, приложите через [pastebin](https://pastebin.com/) полный выхлоп клиента вместе с командой и диагностической информацией:

```
$ clippy hi
--------------------------------------------------------------------------------
Good evening, rlipovsky!

Command running: ['hi'], cwd: /workspace/test-cpp-course/client
Time: 2021-02-03 18:41:48
Platform: Linux-4.19.121-linuxkit-x86_64-with-glibc2.29
C++ compiler: /usr/bin/clang++-11 (Ubuntu clang version 11.0.0-2~ubuntu20.04.1)
Python: 3.8.5, CPython, /workspace/test-cpp-course/client/venv/bin/python
Repository root directory: /workspace/test-cpp-course
Git current commit: 08db1e7bb474f636468951cc0ca92434c5055ec2

...
```

