cmake_minimum_required(VERSION 3.5)

begin_task()
add_task_library(solution wheels)
add_task_test_dir(tests)
end_task()
