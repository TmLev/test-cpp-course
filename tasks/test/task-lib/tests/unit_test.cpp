#include <wheels/test/test_framework.hpp>

#include <solution/solution.hpp>

#include <iostream>

TEST_SUITE(Task) {
  SIMPLE_TEST(Answer) {
    int answer = solution::GetAnswer();
    std::cout << "Answer: " << answer << std::endl;
    ASSERT_EQ(answer, 42);
  }
}

RUN_ALL_TESTS()
