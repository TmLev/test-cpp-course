# test-cpp-course

Репозиторий для разработки / тестирования инфраструктуры:

- [Clippy](https://gitlab.com/Lipovsky/clippy)
- [Pequod](https://gitlab.com/Lipovsky/pequod)

## Инструкции

1) [Начало работы](docs/setup.md)
2) [Как сдавать задачи](docs/ci.md)

## Навигация

- [Задачи](/tasks)
- [Дедлайны](/deadlines)
- [Manytask](http://84.252.128.234:5111/)
